import { Reducer } from "redux";
import { LoadingStatus } from "../globalTypes";
import { CountryDataActionTypes, CountryAction } from "./actions";
import { FeatureCollection } from "geojson";

export type CountryData = {
  country: string;
  regions: RegionData[];
}

export type RegionData = {
  provinceState: string | null;
  countryRegion: string;
  lastUpdate: number;
  lat: number | null;
  long: number | null;
  confirmed: number;
  recovered: number;
  deaths: number;
  active: number;
  admin2: null;
  fips: null;
  combinedKey: string;
  incidentRate: number;
  peopleTested: null;
  peopleHospitalized: null;
  uid: number;
  iso3: string;
  iso2: string;
};

export type CountryState = {
  data: CountryData[];
  loading: LoadingStatus;
  selectedCountry: CountryData | undefined;
  selectedCountryGeodata: FeatureCollection | undefined;
  selectedCountryGeodataStatus: LoadingStatus;
};

const defaultState: CountryState = {
  data: [],
  loading: LoadingStatus.NotStarted,
  selectedCountry: undefined,
  selectedCountryGeodata: undefined,
  selectedCountryGeodataStatus: LoadingStatus.NotStarted
};

export default ((state, action: CountryAction): CountryState => {
  if (!state) {
    return defaultState;
  }

  switch (action.type) {
    case CountryDataActionTypes.SET_COUNTRY_DATA:
      return {
        ...state,
        data: action.data,
        loading: LoadingStatus.Done
      };
    case CountryDataActionTypes.SET_COUNTRY_LOADING_STATUS:
      return {...state, loading: action.status};
    case CountryDataActionTypes.SET_SELECTED_COUNTRY:
      return {
        ...state,
        selectedCountry: action.country,
        selectedCountryGeodata: action.geodata,
        selectedCountryGeodataStatus: LoadingStatus.Done
      };
    case CountryDataActionTypes.SET_SELECTED_COUNTRY_DATA_STATUS:
      return {...state, selectedCountryGeodataStatus: action.status};
    default:
      return state;
  }
}) as Reducer<CountryState, CountryAction>;
