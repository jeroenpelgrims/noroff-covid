import { SetCountryDataAction } from "./setCountryData";
import { SetCountryLoadingStatusAction } from "./setCountryLoadingStatus";
import { SetSelectedCountryDataAction } from "./setSelectedCountryData";
import { SetSelectedCountryDataStatusAction } from "./setSelectedCountryDataStatus";

export enum CountryDataActionTypes {
  SET_COUNTRY_DATA = "SET_COUNTRY_DATA",
  SET_COUNTRY_LOADING_STATUS = "SET_COUNTRY_LOADING_STATUS",
  SET_SELECTED_COUNTRY = "SET_SELECTED_COUNTRY",
  SET_SELECTED_COUNTRY_DATA_STATUS = "SET_SELECTED_COUNTRY_DATA_STATUS",
}

export type CountryAction =
  | SetCountryDataAction
  | SetCountryLoadingStatusAction
  | SetSelectedCountryDataAction
  | SetSelectedCountryDataStatusAction;
