import { groupBy } from "ramda";
import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import { RootState } from "../..";
import { LoadingStatus } from "../../globalTypes";
import { CountryData, RegionData } from '../reducer';
import setCountryData from "./setCountryData";
import setCountryLoadingStatus from "./setCountryLoadingStatus";
import { getDistance } from "geolib";
import loadSelectedCountryData from "./loadSelectedCountryData";

function groupByCountry(regionData: RegionData[]): CountryData[] {
  const byCountry = groupBy(region => region.countryRegion, regionData);
  return Object
    .entries(byCountry)
    .map(([country, regions]) => ({
      country,
      regions
    }));
}

async function findClosestCountry(regionData: RegionData[]): Promise<string> {
  type RegionDistance = { region: RegionData, distance: number };

  return new Promise((resolve, reject) => {

    navigator.geolocation.getCurrentPosition(result => {
      const { coords } = result;
      const closestRegion = regionData
        .filter(x => x.lat && x.long)
        .reduce((closest, region) => {
          const distance = getDistance(
            {lat: coords.latitude, lon: coords.longitude},
            {lat: region.lat!, lon: region.long!}
          );

          if (!closest || distance < closest.distance) {
            return { region, distance };
          }

          return closest;
        }, undefined as RegionDistance | undefined);

      resolve(closestRegion!.region.countryRegion);
    }, reject);
  });
}

export default (): ThunkAction<void, RootState, unknown, AnyAction> => {
  return async dispatch => {
    dispatch(setCountryLoadingStatus(LoadingStatus.InProgress));

    const response = await fetch("https://covid19.mathdro.id/api/recovered");

    if (response.ok) {
      const regionData = await response.json() as RegionData[];
      const countryData = groupByCountry(regionData);

      dispatch(setCountryData(countryData));

      const countryName = await findClosestCountry(regionData);
      const closestCountry = countryData.find(x => x.country === countryName);
      if (closestCountry) {
        dispatch(loadSelectedCountryData(closestCountry));
      }
    } else {
      dispatch(setCountryLoadingStatus(LoadingStatus.Error));
    }
  };
};
