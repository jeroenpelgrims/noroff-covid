import { Action } from "redux";
import { CountryDataActionTypes } from ".";
import { CountryData } from "../reducer";

export interface SetCountryDataAction extends Action<CountryDataActionTypes> {
  type: CountryDataActionTypes.SET_COUNTRY_DATA;
  data: CountryData[];
}

export default (data: CountryData[]): SetCountryDataAction => {
  return { type: CountryDataActionTypes.SET_COUNTRY_DATA, data };
}
