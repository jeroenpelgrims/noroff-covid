import { Action } from "redux";
import { CountryDataActionTypes } from ".";
import { LoadingStatus } from "../../globalTypes";

export interface SetCountryLoadingStatusAction extends Action<CountryDataActionTypes> {
  type: CountryDataActionTypes.SET_COUNTRY_LOADING_STATUS;
  status: LoadingStatus;
}

export default (status: LoadingStatus): SetCountryLoadingStatusAction => {
  return { type: CountryDataActionTypes.SET_COUNTRY_LOADING_STATUS, status };
}
