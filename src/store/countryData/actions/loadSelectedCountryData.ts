import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import { RootState } from "../..";
import { LoadingStatus } from "../../globalTypes";
import { CountryData } from '../reducer';
import setSelectedCountryData from "./setSelectedCountryData";
import camelcase from "camelcase";
import setSelectedCountryDataStatus from "./setSelectedCountryDataStatus";
// import x from "@amcharts/amcharts4-geodata/cote";

export default (country: CountryData): ThunkAction<void, RootState, unknown, AnyAction> => {
  return async dispatch => {
    dispatch(setSelectedCountryDataStatus(LoadingStatus.InProgress));
    const name = camelcase(country.country.toLowerCase())
      .replace("unitedKingdom", "uk")
      .replace("unitedArabEmirates", "uae");

    try {
      const geodata = await import(`@amcharts/amcharts4-geodata/${name}Low`);
      dispatch(setSelectedCountryData(country, geodata.default));
    } catch (e) {
      dispatch(setSelectedCountryDataStatus(LoadingStatus.Error));
    }
  };
};
