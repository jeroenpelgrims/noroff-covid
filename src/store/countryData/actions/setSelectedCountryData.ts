import { Action } from "redux";
import { CountryDataActionTypes } from ".";
import { CountryData } from "../reducer";
import { FeatureCollection } from "geojson";

export interface SetSelectedCountryDataAction extends Action<CountryDataActionTypes> {
  type: CountryDataActionTypes.SET_SELECTED_COUNTRY;
  country: CountryData;
  geodata: FeatureCollection;
}

export default (country: CountryData, geodata: FeatureCollection): SetSelectedCountryDataAction => {
  return {
    type: CountryDataActionTypes.SET_SELECTED_COUNTRY,
    country,
    geodata
  };
}
