import { Action } from "redux";
import { CountryDataActionTypes } from ".";
import { LoadingStatus } from "../../globalTypes";

export interface SetSelectedCountryDataStatusAction extends Action<CountryDataActionTypes> {
  type: CountryDataActionTypes.SET_SELECTED_COUNTRY_DATA_STATUS;
  status: LoadingStatus;
}

export default (status: LoadingStatus): SetSelectedCountryDataStatusAction => {
  return { type: CountryDataActionTypes.SET_SELECTED_COUNTRY_DATA_STATUS, status };
}
