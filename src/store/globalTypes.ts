export enum LoadingStatus {
  NotStarted,
  InProgress,
  Done,
  Error
}
