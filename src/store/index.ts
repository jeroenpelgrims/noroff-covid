import { createStore, applyMiddleware, combineReducers } from "redux";
import countryDataReducer from "./countryData/reducer";
import contactReducer from "./contact/reducer";
import thunk from "redux-thunk";
import { composeWithDevTools } from 'redux-devtools-extension'

const store = createStore(
  combineReducers({
    country: countryDataReducer,
    contact: contactReducer
  }),
  composeWithDevTools(applyMiddleware(thunk))
);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
