import {
  TypedUseSelectorHook,
  useSelector as useSelector_,
  useDispatch as useDispatch_
} from "react-redux";
import { RootState } from ".";

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector_;
export const useAppDispatch = () => useDispatch_();
