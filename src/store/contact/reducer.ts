import { Reducer } from "redux";
import { LoadingStatus } from "../globalTypes";
import { ContactActionTypes, ContactAction } from "./actions";

export type ContactState = {
  contactStatus: LoadingStatus;
};

const defaultState: ContactState = {
  contactStatus: LoadingStatus.NotStarted
};

export default ((state, action: ContactAction): ContactState => {
  if (!state) {
    return defaultState;
  }

  switch (action.type) {
    case ContactActionTypes.SET_CONTACT_STATUS:
      return {...state, contactStatus: action.status};
    default:
      return state;
  }
}) as Reducer<ContactState, ContactAction>;
