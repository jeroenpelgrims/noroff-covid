import { SetContactStatusAction } from "./setContactStatus";

export enum ContactActionTypes {
  SET_CONTACT_STATUS = "SET_CONTACT_STATUS"
}

export type ContactAction =
  | SetContactStatusAction;
