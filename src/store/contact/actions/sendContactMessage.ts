import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import { RootState } from "../..";
import { LoadingStatus } from "../../globalTypes";
import setContactStatus from "./setContactStatus";

export type ContactData = {
  name: string;
  email: string;
  message: string;
}

export default (data: ContactData): ThunkAction<void, RootState, unknown, AnyAction> => {
  return async dispatch => {
    await dispatch(setContactStatus(LoadingStatus.InProgress));

    // I'm simulating an api call here
    setTimeout(() => {
      // send contact message here
      console.log("Sent contact info:", data);

      const responseOk = true;

      if (responseOk) {
        dispatch(setContactStatus(LoadingStatus.Done));

        setTimeout(
          () => dispatch(setContactStatus(LoadingStatus.NotStarted)),
          5000
        );
      } else {
        dispatch(setContactStatus(LoadingStatus.Error));
      }
    }, 2000);
  };
};
