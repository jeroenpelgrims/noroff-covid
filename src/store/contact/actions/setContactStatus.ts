import { Action } from "redux";
import { ContactActionTypes } from ".";
import { LoadingStatus } from "../../globalTypes";

export interface SetContactStatusAction extends Action<ContactActionTypes> {
  type: ContactActionTypes.SET_CONTACT_STATUS;
  status: LoadingStatus;
}

export default (status: LoadingStatus): SetContactStatusAction => {
  return { type: ContactActionTypes.SET_CONTACT_STATUS, status };
}
