import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import loadCountryData from "../../store/countryData/actions/loadCountryData";
import CountrySelector from "../CountrySelector";
import Message from "../Message";
import Loader from "../Loader";
import CountryDashboard from "../CountryDashboard";
import "./style.scss";
import Footer from "../Footer";
import { LoadingStatus } from "../../store/globalTypes";
import loadSelectedCountryData from "../../store/countryData/actions/loadSelectedCountryData";

function App() {
  const {
    loading,
    data: countryData,
    selectedCountry,
    selectedCountryGeodata,
    selectedCountryGeodataStatus
  } = useAppSelector(s => s.country);
  const dispatch = useAppDispatch();

  useEffect(() => { dispatch(loadCountryData()); }, [dispatch]);

  if (loading === LoadingStatus.Error) {
    return (
      <Message
        type="error"
        message="An error has occurred while loading the data"
      />
    );
  }

  if (loading === LoadingStatus.InProgress) {
    return <Loader />;
  }

  return (
    <main>
      <CountrySelector
        countryData={countryData}
        setSelectedCountry={country => dispatch(loadSelectedCountryData(country))}
      />

      <CountryDashboard
        country={selectedCountry}
        geodata={selectedCountryGeodata}
        geodataStatus={selectedCountryGeodataStatus}
      />

      <Footer/>
    </main>
  );
}

export default App;
