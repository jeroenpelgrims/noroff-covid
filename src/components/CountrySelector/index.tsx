import React, { useState } from "react";
import Autosuggest from 'react-autosuggest';
import { CountryData } from "../../store/countryData/reducer";
import "./style.scss";

type Props = {
  countryData: CountryData[];
  setSelectedCountry: (country: CountryData) => void;
};

export default ({ countryData, setSelectedCountry }: Props) => {
  const [value, setValue] = useState<string>("");
  const [suggestions, setSuggestions] = useState<CountryData[]>([]);

  function fillSuggestions(query: string) {
    const formattedQuery = query.toLowerCase();
    const matchingCountries = countryData.filter(
      x => x.country.toLowerCase().includes(formattedQuery)
    );
    setSuggestions(matchingCountries);
  }

  return (
    <Autosuggest
      suggestions={suggestions}
      onSuggestionsFetchRequested={e => fillSuggestions(e.value)}
      onSuggestionsClearRequested={() => setSuggestions([])}
      getSuggestionValue={x => x.country}
      renderSuggestion={x => <>{x.country}</>}
      inputProps={{
        value,
        placeholder: "Type a country name",
        onChange: (_, { newValue }) => setValue(newValue),
      }}
      highlightFirstSuggestion={true}
      onSuggestionSelected={
        (_, { suggestion }) => setSelectedCountry(suggestion)
      }
    />
  )
};
