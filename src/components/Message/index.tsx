import React from "react";
import "./style.scss";

type Props = { message: string, type: "error" | "success" };
export default ({ message, type }: Props) => {
  return (
    <div className={`message ${type}`}>
      {message}
    </div>
  );
};
