import React, { MouseEvent, useState } from "react";
import ContactForm from "./ContactForm";
import "./style.scss";

export default () => {
  const [showForm, setShowForm] = useState<boolean>(false);

  function toggleContactForm(e: MouseEvent<HTMLAnchorElement>) {
    e.preventDefault();
    setShowForm(!showForm);
  }

  return (
    <footer>
      <ul>
        <li>
          Made by&nbsp;
          <a href="https://jeroenpelgrims.com" target="_blank" rel="noreferrer">
            Jeroen Pelgrims
          </a>
        </li>
        <li>
          <a
            href="https://gitlab.com/jeroenpelgrims/noroff-covid"
            target="_blank"
            rel="noreferrer"
          >
            Source
          </a>
        </li>
        <li>
          <a href="#" onClick={toggleContactForm}>Contact</a>
        </li>
      </ul>

      {showForm ? <ContactForm/> : null}
    </footer>
  );
};
