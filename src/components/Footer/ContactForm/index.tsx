import React from "react";
import { Field, Form, Formik } from "formik";
import "./style.scss";
import { useAppDispatch, useAppSelector } from "../../../store/hooks";
import { LoadingStatus } from "../../../store/globalTypes";
import sendContactMessage, { ContactData } from "../../../store/contact/actions/sendContactMessage";
import Message from "../../Message";

function renderContactStatus(status: LoadingStatus): JSX.Element | null {
  switch(status) {
    case LoadingStatus.NotStarted:
    case LoadingStatus.InProgress:
      return null;
    case LoadingStatus.Error:
      return (
        <Message
          type="error"
          message="An error has occurred while trying to send the message."
        />
      );
    case LoadingStatus.Done:
      return (
        <Message
          type="success"
          message="If I had implemented the api call your message would have been on it's way now!"
        />
      );
  }
}

export default () => {
  const dispatch = useAppDispatch();
  const contactStatus = useAppSelector(s => s.contact.contactStatus);
  const defaultData: ContactData = { name: "", email: "", message: "" };

  return (
    <Formik<ContactData>
      initialValues={defaultData}
      onSubmit={async (values, { resetForm }) => {
        dispatch(sendContactMessage(values));
        resetForm();
      }}
    >
      <Form>
        <h2>Contact form</h2>

        <label>
          <span>Name</span>
          <Field name="name" type="text" required minlength="2" />
        </label>

        <label>
          <span>Email</span>
          <Field name="email" type="email" required />
        </label>

        <label>
          <span>Message</span>
          <Field
            name="message"
            type="text"
            as="textarea"
            required
            minlength="25"
          />
        </label>

        <button
          type="submit"
          disabled={contactStatus === LoadingStatus.InProgress}
        >
          {
            contactStatus === LoadingStatus.InProgress
            ? "Sending message..."
            : "Send message"}
        </button>

        {renderContactStatus(contactStatus)}
      </Form>
    </Formik>
  );
};
