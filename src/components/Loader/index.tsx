import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import "./style.scss";

type Props = {
  inline?: boolean;
};

export default ({ inline }: Props) => {
  return (
    <div className={`loader${inline ? " inline": ""}`}>
      <FontAwesomeIcon icon={faCircleNotch}/>
    </div>
  );
};
