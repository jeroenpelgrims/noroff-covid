import React from "react";
import { CountryData } from "../../store/countryData/reducer";
import { FeatureCollection } from "geojson";
import "./style.scss";
import TotalStats from "./TotalStats";
import Loader from "../Loader";
import CountryMap from "./CountryMap";
import { LoadingStatus } from "../../store/globalTypes";
import Message from "../Message";

type Props = {
  country: CountryData | undefined;
  geodata: FeatureCollection | undefined;
  geodataStatus: LoadingStatus;
};

export default ({ country, geodata, geodataStatus }: Props) => {
  if (country === undefined) {
    return <div className="countryDashboard"/>;
  }

  function renderMap() {
    switch(geodataStatus) {
      case LoadingStatus.NotStarted:
        return null;
      case LoadingStatus.Error:
        return (
          <Message
            type="error"
            message="The map data couldn't be loaded"
          />
        );
      case LoadingStatus.InProgress:
        return <Loader />;
      case LoadingStatus.Done:
        return <CountryMap geodata={geodata!}/>;
    }
  }

  return (
    <div className="countryDashboard">
      <h1>{country.country}</h1>

      {renderMap()}

      <TotalStats country={country} />
    </div>
  );
};
