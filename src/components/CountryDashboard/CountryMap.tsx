import { useLayoutEffect, useRef } from "react";
import { create } from "@amcharts/amcharts4/core";
import { MapChart, projections, MapPolygonSeries } from "@amcharts/amcharts4/maps";
import { FeatureCollection } from "geojson";
import "./style.scss";

type Props = {
  geodata: FeatureCollection;
};

export default ({ geodata }: Props) => {
  const chart = useRef<MapChart | null>(null);

  useLayoutEffect(() => {
    const x = create("chartdiv", MapChart);
    chart.current = x;

    x.seriesContainer.draggable = false;
    x.seriesContainer.resizable = false;
    x.maxZoomLevel = 1;
    x.geodata = geodata;
    x.projection = new projections.Miller();
    var polygonSeries = x.series.push(new MapPolygonSeries());
    polygonSeries.useGeodata = true;

    var polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";

    // function findId(name: string) {
    //   return geodata?.features.find(x => x.properties!["name"] === name)?.id;
    // };

    // polygonSeries.data = country.regions.map(region => {
    //   const id = findId(region.provinceState!);
    //   return {
    //     id,
    //     "name": `${region.provinceState}
    //       test
    //     `,
    //     "value": 100,
    //     "fill": am4core.color("#F05C5C")
    //   };
    // })

    // polygonTemplate.propertyFields.fill = "fill";

    return () => {
      x.dispose();
    };
  });

  return (
    <div id="chartdiv" style={{ width: "100%", height: "500px" }}/>
  );
};
