import React from "react";
import { CountryData } from "../../../store/countryData/reducer";
import { sum } from "ramda";
import Percentage from "./Percentage";

type Props = {
  country: CountryData
};

export default ({ country }: Props) => {
  const confirmed = sum(country.regions.map(x => x.confirmed));
  const deaths = sum(country.regions.map(x => x.deaths));
  const active = sum(country.regions.map(x => x.active));
  const recovered = sum(country.regions.map(x => x.recovered));

  return (
    <ul>
      <li>
        Total confirmed cases: {confirmed}
      </li>
      <li>
        Total active cases: {active}&nbsp;
        (<Percentage numerator={active} denominator={confirmed} />)
      </li>
      <li>
        Total deaths: {deaths}&nbsp;
        (<Percentage numerator={deaths} denominator={confirmed} />)
      </li>
      <li>
        Total recovered: {recovered}&nbsp;
        (<Percentage numerator={recovered} denominator={confirmed} />)
      </li>
    </ul>
  );
};
