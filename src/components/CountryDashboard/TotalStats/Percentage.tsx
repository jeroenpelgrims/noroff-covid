import React from "react";

type Props = {
  numerator: number,
  denominator: number;
  decimals?: number;
};

export default ({ numerator, denominator, decimals }: Props) => {
  const percentage = numerator / denominator * 100;
  const decimalRatio = Math.pow(10, decimals || 2);
  const roundedPercentage = Math.round(percentage * decimalRatio) / decimalRatio;

  return <>{roundedPercentage}%</>;
};
